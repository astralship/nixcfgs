{ config, pkgs, lib, inputs, ...}:
{
  # This makes it so that nix channels and registries are pure, and set to use
  # the nixpkgs from our flake.nix input. This means there is no impurity
  # possibilities by default. For example, `nix-shell -p hello` will now get
  # this package from the nixpkgs defined in our flake.nix, rather than from
  # some nix-channel set by the user, or from the flake registry when using
  # `nix shell nixpkgs#hello`.
  nix = {
    extraOptions =
      let empty_registry = builtins.toFile "empty-flake-registry.json" ''{"flakes":[],"version":2}''; in
      ''
        experimental-features = nix-command flakes
        flake-registry = ${empty_registry}
      '';
    trustedUsers = [ "@wheel" "root" ];
    registry.nixpkgs.flake = inputs.nixpkgs;
    nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
  };
  environment.systemPackages = [ pkgs.git ];
}
