# nixcfgs

Configuration for all machines aboard the Astralship.

To switch to any configuration, on any machine, replace `<machine>` with the
name of the `nixosConfiguration` you wish to switch to, for example `nasa`.

```
# Example: 
# nixos-rebuild switch --flake .#nasa

nixos-rebuild switch --flake .#<machine>
```
