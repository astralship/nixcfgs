{ config, pkgs, ... }:

{  
  imports =
    [
      ./hardware-configuration.nix
      ../../../profiles/blenderMachine.nix
      ../../../profiles/nvidia.nix
      ../../../profiles/gui.nix
      ../../../mixins/pipewire.nix
    ];

  powerManagement.enable = false;

  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "6264bbdb";

  boot.loader.efi.canTouchEfiVariables = false;
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.efiInstallAsRemovable = true;
  
  boot.loader.grub.mirroredBoots = [
    { devices = [ "/dev/disk/by-uuid/5F1B-7FB8" ];
      path = "/boot-fallback"; }
  ];

  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;

  networking.hostName = "fu1";
}

