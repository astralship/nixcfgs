# {
#   containers.database = {
#     config = 
#       { config, pkgs, ... }: 
#       { 
#         imports = [ import ./avahi.nix ]; 
#       };
#     privateNetwork = false;
#     hostAddress = "192.168.3.216";
#     localAddress = "192.168.100.11";
#   };
# }


#{
#  containers.postgresql = {
#    autoStart = true;
#    ephemeral = true;
#    config = { config, lib, pkgs, ... }: {
#      imports = [
#        <nixpkgs/nixos/modules/profiles/headless.nix>
#      ];
#      services.postgresql.enable = true;
#      services.postgresql.ensureDatabases = [ "gitea" "nextcloud" ];
#      services.postgresql.ensureUsers = [{
#        name = "gitea";
#        ensurePermissions = { "DATABASE gitea" = "ALL PRIVILEGES"; };
#      }];
#    };
#}

{ ... }: {
  containers.database = {
    autoStart = true;
    hostAddress = "192.168.3.216";
    localAddress = "192.168.100.11";
    config = { config, pkgs, ... }: {
      services.postgresql.enable = true;
      imports = [ ./avahi.nix ];
      networking.hostName = "foobarish";
      services.postgresql.package = pkgs.postgresql_9_6;
    };
  };
}
