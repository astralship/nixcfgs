# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, inputs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./modules/nextcloud.nix
      ./modules/postgresbackup.nix
      ./modules/log-to-tty1.nix
      ./modules/dataGathering.nix
      ./modules/tailscale.nix
      ./modules/samba.nix
      "${inputs.self}/mixins/avahi.nix"
      "${inputs.self}/mixins/pirates.nix"
      "${inputs.self}/mixins/nixUnstable.nix"
      "${inputs.self}/mixins/common.nix"
    ];

  # Some details of users are imported from pirates.nix above, but not
  # everything is defined in pirates.nix. This is merged with pirates.nix.
  users.users.l33 = {
    extraGroups = [ "wheel" "lp" "scanner" "disk" "audio" "video" ];
    uid = 1012;
  };

  nixpkgs.config.allowUnfree = true;

  boot = {
    supportedFilesystems = [ "zfs" "ntfs" "fat32" ];
    loader = {
      efi.canTouchEfiVariables = false;
      systemd-boot = {
        enable = true;
        configurationLimit = 10;
      };
    };
  };

  networking = {
    # networking.hostId must be set for ZFS.
    hostId = "deadcafe";
    useDHCP = false;
    interfaces = {
      enp1s0f0 = {
        useDHCP = true;
        ipv4.addresses = [{ address = "192.168.1.114"; prefixLength = 24; }];
      };
    };
    nameservers = [ "192.168.1.2" ];
    hostName = "nasa";
    firewall = {
      allowedTCPPorts = [ 80 443 999 ];
    };
  };

  time.timeZone = "Europe/London";

  environment.systemPackages = with pkgs; [
    wget
    vim
    tmux
    git
    lsb-release
    usbutils
    zfs
    rsync 
  ];

  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  services.openssh.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}

