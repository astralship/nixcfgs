# Configures Nextcloud, which includes modifying/optimizing nginx and
# postgresql to suit our needs.

{ config, pkgs, lib, ... }:
{
  systemd.services."nextcloud-setup" = {
    requires = [ "postgresql.service" ];
    after = [ "postgresql.service" ];
  };

  services = {

    postgresql = {
      enable = true;
      # Ensure the database, user, and permissions always exist
      ensureDatabases = [ "nextcloud" ];
      ensureUsers = [{
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }];
    };

    nginx = {
      enable = true;
      commonHttpConfig = ''
        # Optimisation
        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 600;
   
        proxy_redirect          off;
        proxy_connect_timeout   600;
        proxy_send_timeout      600;
        proxy_read_timeout      600;
        proxy_http_version      1.0; 
  
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;
        proxy_set_header        X-Forwarded-Host $host;
        proxy_set_header        X-Forwarded-Server $host;
        proxy_set_header        Accept-Encoding ""; 
      '';
  
      # Use recommended settings
      recommendedGzipSettings = true;
      recommendedOptimisation = false; # disable defaults and set them in commonHttpConfig
      recommendedProxySettings = false;
      recommendedTlsSettings = true;
  
      # Only allow PFS-enabled ciphers with AES256
      sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
  
      # Setup Nextcloud virtual host to listen on ports
      virtualHosts = {
        "nasa.astral" = {
          ## Force HTTP redirect to HTTPS
          forceSSL = true;
          ## LetsEncrypt
          enableACME = false;
          sslCertificateKey = "/var/nginx-selfsigned.key";
          sslCertificate = "/var/nginx-selfsigned.crt";
        };
      };
    };

    nextcloud = {
      package = pkgs.nextcloud22;
      enable = true;
      # By setting hostName, we enable built-in virtual host management, without having to use services.nginx.
      # This takes care of what would otherwise be a more complicated setup.
      # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/services/web-apps/nextcloud.nix#L529
      hostName = "nasa.astral";
      maxUploadSize = "32000M"; # increase max upload file size
      poolSettings = {
        "pm" = "dynamic";
        "pm.max_children" = "2048";
        "pm.start_servers" = "4";
        "pm.min_spare_servers" = "4";
        "pm.max_spare_servers" = "8";
        "pm.max_requests" = "1000";
      };
      # Use HTTPS for links
      https = true;
      # Auto-update Nextcloud Apps
      autoUpdateApps.enable = true;
      # Set what time makes sense for you
      autoUpdateApps.startAt = "05:00:00";
      config = {
        # Further forces Nextcloud to use HTTPS
        overwriteProtocol = "https";
  
        # Nextcloud PostegreSQL database configuration, recommended over using SQLite
        dbtype = "pgsql";
        dbuser = "nextcloud";
        dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
        dbname = "nextcloud";
        dbpassFile = "/var/nextcloud-db-pass";
  
        adminpassFile = "/var/nextcloud-admin-pass";
        adminuser = "admin";

        extraTrustedDomains = [ "100.76.70.117" "nasa.astral" ];
      };
    };

  };
}

# passwords for admin interface and db stored in /var/nextcloud-db-pass /var/nextcloud-admin-pass
