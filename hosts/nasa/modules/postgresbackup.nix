# Backs the postgresql database up daily at 2:30:00.

{ config, pkgs, lib, ... }:
{
  systemd.services.dbBackup = {
    serviceConfig.Type = "oneshot";
    serviceConfig.User = "postgres";
    script = ''
      today=$(date +"%Y%m%d")
      # Dump the database into zstd via pipe, then redirect that to a dump file.
      ${config.services.postgresql.package}/bin/pg_dumpall | ${pkgs.zstd}/bin/zstd > /var/backup/postgresql/postgres_nextcloudastral.$today.dump.zst
    '';
  };

  systemd.timers.dbBackup = {
    wantedBy = [ "timers.target" ];
    partOf = [ "dbBackup.service" ];
    timerConfig.OnCalendar = "*-*-* 2:30:00";
    timerConfig.Persistent = true;
  };
}
