{
  networking.firewall.allowedTCPPorts = [ 1883 3000 8086 1880 ];
  imports = [ ../../../mixins/mosquitto.nix ];
  services = {
    influxdb = {
      enable = true;
    };
    node-red = {
      enable = true;
      withNpmAndGcc = true;
    };
    grafana = {
      enable = true;
      addr = "0.0.0.0";
      auth.anonymous.enable = true;
    };
  };
}
