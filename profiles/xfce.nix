{ pkgs, lib, config, ... }:

{
  config = {
    # Disable auto-suspend
    systemd = {
      targets.sleep.enable = false;
      targets.suspend.enable = false;
      targets.hibernate.enable = false;
      targets.hybrid-sleep.enable = false;
    };
    networking.networkmanager = {
      enable = true;
      unmanaged = [ "wlp4s0" ];
    };
    xdg = {
      portal.enable = true;
      portal.gtkUsePortal = true;
      portal.extraPortals = with pkgs;
        [ xdg-desktop-portal-wlr /*xdg-desktop-portal-gtk*/ ];
    };
    services = {
      xserver = {
        enable = true;
        displayManager.gdm.enable = true;
        desktopManager.xfce = {
          enable = true;
        };
      };
    };
  };
}
