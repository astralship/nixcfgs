{ pkgs, lib, config, ... }:

{
  config = {
    # Disable the GNOME3/GDM auto-suspend feature that cannot be disabled in GUI!
    # If no user is logged in, the machine will power down after 20 minutes.
    systemd = {
      targets.sleep.enable = false;
      targets.suspend.enable = false;
      targets.hibernate.enable = false;
      targets.hybrid-sleep.enable = false;
    };
    networking.networkmanager.unmanaged = [ "wlp4s0" ];
    nixpkgs.config.firefox.enableGnomeExtensions = true;
    services = {
      xserver = {
        enable = true;
        displayManager.gdm.enable = true;
#        desktopManager.gnome.enable = true;
        desktopManager.gnome = {
          enable = true;
          extraGSettingsOverridePackages = with pkgs; [ gnome3.gnome-settings-daemon ];
          extraGSettingsOverrides = ''
            [org.gnome.desktop.screensaver]
            lock-delay=3600
            lock-enabled=true'
        
            [org.gnome.desktop.session]
            idle-delay=900
        
            [org.gnome.settings-daemon.plugins.power]
            power-button-action='nothing'
            idle-dim=true
            sleep-inactive-battery-type='nothing'
            sleep-inactive-ac-timeout=3600
            sleep-inactive-ac-type='nothing'
            sleep-inactive-battery-timeout=1800
          '';
    };
      };
      gnome = {
        core-os-services.enable = true;
        tracker.enable = false;
        sushi.enable = false;
        rygel.enable = false;
        gnome-initial-setup.enable = false;
        tracker-miners.enable = false;
        evolution-data-server.enable = lib.mkForce false;
      };
    };
    xdg = {
      portal.enable = true;
      portal.gtkUsePortal = true;
      portal.extraPortals = with pkgs;
        [ xdg-desktop-portal-wlr /*xdg-desktop-portal-gtk*/ ];
    };
    programs = {
      gnome-disks.enable = true;
      evince.enable = true;
      file-roller.enable = true;
      geary.enable = false;
    };
    environment.gnome.excludePackages = [
      pkgs.gnome-tour
      pkgs.gnome.gnome-weather
      pkgs.gnome.cheese
      pkgs.gnome.epiphany
      pkgs.gnome.yelp
      pkgs.gnome.gnome-maps
      pkgs.gnome.gnome-initial-setup
      pkgs.gnome.gnome-contacts
      pkgs.gnome-photos
      pkgs.gnome.gnome-calendar
      pkgs.gnome.gnome-control-center
    ];
  };
}
