{ config, pkgs, lib, ... }:
{
  environment.systemPackages = with pkgs; [
    firefox
    qbittorrent
    mpv
  ];
}
