{ config, lib, pkgs, ... }:
let
  blender3WithOptix = pkgs.blender.override { cudaSupport = true; };
in
{
  imports = [ ./xfce.nix ];
  services.xserver.videoDrivers = [ "nvidia" ];
  environment.systemPackages = with pkgs; [
    blender3WithOptix
  ];
}
