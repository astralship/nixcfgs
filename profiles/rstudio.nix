{ pkgs, lib, config, ... }:
{
  environment.systemPackages = [
    (pkgs.rstudioWrapper.override {
      packages = with pkgs.rPackages; [
        shiny
      ];
    })
  ];
}
